# MyExample

[![Build Status](https://travis-ci.com/i.fons/MyExample.jl.svg?branch=main)](https://travis-ci.com/i.fons/MyExample.jl)
[![Build Status](https://ci.appveyor.com/api/projects/status/github/i.fons/MyExample.jl?svg=true)](https://ci.appveyor.com/project/i.fons/MyExample-jl)
[![Coverage](https://codecov.io/gh/i.fons/MyExample.jl/branch/main/graph/badge.svg)](https://codecov.io/gh/i.fons/MyExample.jl)
[![Coverage](https://coveralls.io/repos/github/i.fons/MyExample.jl/badge.svg?branch=main)](https://coveralls.io/github/i.fons/MyExample.jl?branch=main)
