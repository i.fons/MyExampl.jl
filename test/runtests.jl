using MyExample
using Test


@testset "MyExample.jl" begin
    @test my_fun(2,1) == 7
    @test my_fun(2,3) == 13
end